use crate::tokenizers::utils::STOPPING_WORDS;
use crate::tokenizers::Tokenizer;

#[derive(Debug, Clone)]
pub struct SplitTokenizer;

impl Tokenizer for SplitTokenizer {
    fn tokenize(&self, text: &str) -> Option<Vec<String>> {
        let words = text
            .split(" ")
            .filter(|word| !STOPPING_WORDS.contains(&word))
            .map(|word| word.to_lowercase())
            .collect();

        Some(words)
    }

    fn box_clone(&self) -> Box<dyn Tokenizer> {
        Box::new((*self).clone())
    }
}
