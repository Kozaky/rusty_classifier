use crate::tokenizers::utils::{MATCH_WORDS, STOPPING_WORDS};
use crate::tokenizers::Tokenizer;
use algorithmia::Algorithmia;
use regex::Regex;

#[derive(Debug, Clone)]
pub struct LemmaTokenizer {
    pub lemmatizer_api_key: String,
}

impl Tokenizer for LemmaTokenizer {
    fn tokenize(&self, text: &str) -> Option<Vec<String>> {
        let mut optional_tokenized_text = None;

        let re = Regex::new(&MATCH_WORDS).unwrap();

        if let Some(lemmatized_text) = self.lemmatize(text) {
            let words: Vec<String> = re
                .find_iter(&lemmatized_text)
                .map(|re_match| re_match.as_str().to_lowercase())
                .filter(|word| !STOPPING_WORDS.contains(&word.as_str()))
                .collect();

            optional_tokenized_text = Some(words);
        }

        optional_tokenized_text
    }

    fn box_clone(&self) -> Box<dyn Tokenizer> {
        Box::new((*self).clone())
    }
}

impl LemmaTokenizer {
    fn lemmatize(&self, word: &str) -> Option<String> {
        let client = Algorithmia::client(self.lemmatizer_api_key.as_str());
        let response = client
            .algo("StanfordNLP/Lemmatizer/0.1.0")
            .pipe(word)
            .unwrap();

        response.into_string()
    }
}
