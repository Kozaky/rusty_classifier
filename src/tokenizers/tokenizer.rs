pub trait Tokenizer: Sync + Send {
    fn tokenize(&self, text: &str) -> Option<Vec<String>>;
    fn box_clone(&self) -> Box<dyn Tokenizer>;
}

impl Clone for Box<dyn Tokenizer> {
    fn clone(&self) -> Box<dyn Tokenizer> {
        self.box_clone()
    }
}
