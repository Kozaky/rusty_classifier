use crate::freq_computer::compute_frequencies;
use crate::tokenizers::{split_tokenizer::SplitTokenizer, Tokenizer};
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, Mutex};
use std::thread;

type Category = String;
const MIN_PROBABILITY: f32 = 0.001;

#[derive(Clone)]
pub struct TextClassifier {
    tokenizer: Box<dyn Tokenizer>,
    freq_dictionary: HashMap<Category, HashMap<String, f32>>,
}

impl TextClassifier {
    pub fn classify(&self, text: &str) -> Option<Category> {
        let mut optional_category = None;

        if let Some(tokenized_text) = self.tokenizer.tokenize(text) {
            let (most_likely_category, _) = self
                .freq_dictionary
                .iter()
                .map(|(category, probabilities)| {
                    let category_probability: f32 = tokenized_text
                        .iter()
                        .map(|word| match probabilities.contains_key(word) {
                            true => probabilities.get(word).unwrap().to_owned(),
                            false => MIN_PROBABILITY,
                        })
                        .product();
                    (category, category_probability)
                })
                .max_by(|(_, probability1), (_, probability2)| {
                    probability1.partial_cmp(probability2).unwrap()
                })
                .unwrap();

            optional_category = Some(most_likely_category.to_owned());
        }

        optional_category
    }
}

pub struct TextClassifierBuilder<'a> {
    tokenizer: Box<dyn Tokenizer>,
    training_data: Option<Vec<(String, Category)>>,
    csv_file_path: Option<&'a str>,
}

impl<'a> TextClassifierBuilder<'a> {
    pub fn new() -> Self {
        Self {
            tokenizer: Box::new(SplitTokenizer),
            training_data: None,
            csv_file_path: None,
        }
    }

    pub fn tokenizer(mut self, tokenizer: Box<dyn Tokenizer>) -> Self {
        self.tokenizer = tokenizer;
        self
    }

    pub fn csv_file_path(mut self, csv_file_path: &'a str) -> Self {
        self.csv_file_path = Some(csv_file_path);
        self
    }

    fn load_csv_file(&mut self) -> Result<(), BuildError> {
        let mut csv_reader = csv::Reader::from_path(self.csv_file_path.unwrap())?;
        let mut records = csv_reader.records().peekable();

        if records.peek().is_none() {
            return Err(BuildError::from(
                "Expected at least one record in the CSV file",
            ));
        } else {
            let mut training_data: Vec<(String, String)> = Vec::new();
            for record in records {
                let record = record?;
                training_data.push((record[0].to_owned(), record[1].to_owned()));
            }
            self.training_data = Some(training_data);
        }

        Ok(())
    }

    fn build_vocabulary(
        &self,
        dictionary: &HashMap<Category, HashMap<String, f32>>,
    ) -> HashSet<String> {
        dictionary
            .values()
            .into_iter()
            .flat_map(|map| map.keys().map(|k| k.to_owned()))
            .collect()
    }

    fn build_dictionary(&self) -> HashMap<String, HashMap<String, f32>> {
        let mut dictionary = self.build_count_dictionary();

        let vocabulary = self.build_vocabulary(&dictionary);
        let frequencies = compute_frequencies(&dictionary, &vocabulary);

        self.build_frequency_dictionary(&mut dictionary, vocabulary, frequencies);

        dictionary
    }

    fn build_count_dictionary(&self) -> HashMap<Category, HashMap<String, f32>> {
        let dictionary_arc: Arc<Mutex<HashMap<Category, HashMap<String, f32>>>> =
            Arc::new(Mutex::new(HashMap::new()));
        let mut handles = vec![];

        for data in self.training_data.as_ref().unwrap().iter() {
            let (text, category) = (data.0.to_owned(), data.1.to_owned());

            let dictionary_arc_clone = Arc::clone(&dictionary_arc);
            let tokenizer = self.tokenizer.clone();
            handles.push(thread::spawn(move || {
                if let Some(tokenized_text) = tokenizer.tokenize(&text) {
                    let mut dictionary = dictionary_arc_clone.lock().unwrap();
                    let category_dictionary = dictionary.entry(category).or_insert(HashMap::new());

                    for word in tokenized_text.iter() {
                        let counter = category_dictionary.entry(word.to_owned()).or_insert(0f32);
                        *counter += 1f32;
                    }
                }
            }));
        }

        for process in handles {
            process.join().unwrap();
        }

        Arc::try_unwrap(dictionary_arc)
            .unwrap()
            .into_inner()
            .unwrap()
    }

    fn build_frequency_dictionary(
        &self,
        dictionary: &mut HashMap<
            std::string::String,
            std::collections::HashMap<std::string::String, f32>,
        >,
        vocabulary: HashSet<String>,
        frequencies: Vec<f32>,
    ) {
        let mut frequencies = frequencies.chunks(frequencies.len() / dictionary.keys().len());

        for (_, category_dictionary) in dictionary {
            let category_freq: HashMap<&String, &f32> =
                vocabulary.iter().zip(frequencies.next().unwrap()).collect();

            for (word, &frequency) in category_freq {
                if category_dictionary.contains_key(word) {
                    category_dictionary.insert(word.to_owned(), frequency);
                }
            }
        }
    }

    pub fn build(mut self) -> Result<TextClassifier, BuildError> {
        if self.csv_file_path.is_none() {
            return Err(BuildError::from("A CSV file path is needed."));
        }

        self.load_csv_file()?;

        let freq_dictionary = self.build_dictionary();

        Ok(TextClassifier {
            tokenizer: self.tokenizer,
            freq_dictionary: freq_dictionary,
        })
    }
}

#[derive(Debug)]
pub struct BuildError {
    description: String,
}

impl From<&str> for BuildError {
    fn from(from: &str) -> Self {
        Self {
            description: from.to_owned(),
        }
    }
}

impl From<csv::Error> for BuildError {
    fn from(from: csv::Error) -> Self {
        Self {
            description: format!("An error ocurred reading the CSV file: {}", from),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tokenizers::lemma_tokenizer::LemmaTokenizer;
    use std::env;
    use std::time::Instant;

    #[test]
    fn it_works() {
        let lemma_tokenizer = LemmaTokenizer {
            lemmatizer_api_key: env::var("LEMMATIZER_API_KEY")
                .expect("An API key for Algorithmia must be provided"),
        };

        let before = Instant::now();
        let text_classifier = TextClassifierBuilder::new()
            .tokenizer(Box::new(lemma_tokenizer))
            .csv_file_path("demo_data.csv")
            .build()
            .unwrap();

        let elapsed = before.elapsed();
        println!("It took {:.2?} to build the TextClassifier", elapsed);

        let mut csv_reader = csv::Reader::from_path("check_demo.csv").unwrap();

        let mut right_guess_counter = 0f32;
        let mut total = 0f32;
        for record in csv_reader.records() {
            let record = record.unwrap();

            let before = Instant::now();
            if text_classifier.classify(&record[0]) == Some(record[1].to_owned()) {
                let elapsed = before.elapsed();
                println!(
                    "It took {:.2?} to classify the text number {:?}",
                    elapsed, total
                );

                right_guess_counter += 1.0;
            } else {
                println!(
                    "TextClassifier was not able to guess the next sentence: {}, [{}]",
                    &record[0], &record[1]
                );
            }
            total += 1.0
        }

        println!("Accuracy: {}", (right_guess_counter / total) as f32);
        println!("Total records: {}", total);
        println!("Total guessed: {}", right_guess_counter);
        println!("Total wrong: {}", total - right_guess_counter);
    }
}
