use std::collections::{HashMap, HashSet};

pub fn compute_frequencies(
    dictionary: &HashMap<String, HashMap<String, f32>>,
    vocabulary: &HashSet<String>,
) -> Vec<f32> {
    let counts: Vec<f32> = dictionary
        .iter()
        .map(|(_, category_dictionary)| {
            vocabulary
                .iter()
                .map(|word| match category_dictionary.contains_key(word) {
                    true => category_dictionary.get(word).unwrap().to_owned(),
                    false => 0f32,
                })
                .collect::<Vec<f32>>()
        })
        .collect::<Vec<Vec<f32>>>()
        .concat();

    let num_rows = dictionary.keys().len() as u64;
    let num_cols = vocabulary.len() as u64;

    let array = arrayfire::Array::new(&counts, arrayfire::Dim4::new(&[num_rows, num_cols, 1, 1]));
    let sum_array = arrayfire::sum(&array, 1);
    let ones_array = arrayfire::Array::new(
        &vec![1f32; num_cols as usize],
        arrayfire::Dim4::new(&[1, num_cols, 1, 1]),
    );
    let sum_array = arrayfire::matmul(
        &sum_array,
        &ones_array,
        arrayfire::MatProp::NONE,
        arrayfire::MatProp::NONE,
    );
    let freq_array = arrayfire::div(&array, &sum_array, false);

    let mut frequencies: Vec<f32> = vec![0.0; vocabulary.len() * dictionary.keys().len()];
    freq_array.host(&mut frequencies);

    frequencies
}
